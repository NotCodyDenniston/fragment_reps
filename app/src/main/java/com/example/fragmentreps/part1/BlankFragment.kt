package com.example.fragmentreps.part1

import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.navigation.fragment.findNavController
import com.example.fragmentreps.R


class BlankFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        println("CREATE")
         }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
               Text(text = "Real empty here.... you should check the terminal!")
            }
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("VIEWCREATED")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        println("VIEWSTATERESTORED")
    }

    override fun onStart() {
        super.onStart()
        println("START")

    }

    override fun onResume() {
        super.onResume()
        println("RESUME")
    }

    override fun onPause() {
        super.onPause()
        println("PAUSE")
    }

    override fun onStop() {
        super.onStop()
        println("STOP")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        println("SAVEINSTANCESTATE")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        println("DESTROYVIEW")
    }

    override fun onDestroy() {
        super.onDestroy()
        println("DESTROY")
    }

}