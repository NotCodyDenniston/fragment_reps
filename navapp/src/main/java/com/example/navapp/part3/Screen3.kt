package com.example.navapp.part3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.navapp.R
import com.example.navapp.ui.theme.FragmentRepsTheme

class Screen3: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                FragmentRepsTheme() {
                    Box(modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .background(
                            Brush.verticalGradient(0f to Color.Black, 1000f to Color.White)),
                    Alignment.Center) {
                        Column() {
                        Text(text = "Screen 3")
                        Button(onClick = { findNavController().navigate(R.id.fragment_screen2) }) {
                            Text(text = "screen 2")
                        }
                        Button(onClick = { findNavController().navigate(R.id.fragment_screen4) }) {
                            Text(text = "screen 4")
                        }
                        }
                    }
                }
            }
        }
    }
}